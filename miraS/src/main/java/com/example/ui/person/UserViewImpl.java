//package com.example.ui.person;
//
//import com.example.entities.User;
//import com.example.ui.user.UserPresenter;
//import com.example.ui.user.UserView;
//import com.vaadin.data.Binder;
//import com.vaadin.data.ValidationException;
//import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
//import com.vaadin.server.FontAwesome;
//import com.vaadin.ui.Alignment;
//import com.vaadin.ui.Button;
//import com.vaadin.ui.FormLayout;
//import com.vaadin.ui.HorizontalLayout;
//import com.vaadin.ui.TextField;
//import com.vaadin.ui.UI;
//import com.vaadin.ui.VerticalLayout;
//import com.vaadin.ui.Button.ClickEvent;
//import com.vaadin.ui.Button.ClickListener;
//
//public class UserViewImpl extends VerticalLayout implements UserView{
//	private static final long serialVersionUID = 1L;
//	
//	private TextField firstName = new TextField("First Name");
//	private TextField lastName = new TextField("Last Name");
//	private TextField age = new TextField("Age");
//	private Button btnSave = new Button("Save");
//	private Button btnDelete = new Button("Delete");
//	private Button btnClose = new Button("Close");
//	private UserPresenter presenter;
//	private Binder<User> binder = new Binder<>(User.class);
//	private User bean;
//	
//	
//	
//	public UserViewImpl() {
//		this.presenter = new UserPresenter(this);
//		buildUI();
//		bean = new User();
//		this.binder.forField(firstName).withNullRepresentation("").bind(User::getUserName, User::setUserName);
//		this.binder.forField(lastName).withNullRepresentation("").bind(User::getPassword, User::setPassword);
//		this.binder.forField(age).withNullRepresentation("").withConverter(
//			    Integer::valueOf,
//			    String::valueOf).bind(User::getId, User::setId);
//	}
//	
//
//	private void buildUI() {
//		setMargin(true);
//		
//		btnSave.addClickListener(event -> doSave());
//		btnDelete.addClickListener(event -> doDelete());
//
//		
//		FormLayout fl = new FormLayout();
//		
//		btnClose.addClickListener(event -> UI.getCurrent().getNavigator().navigateTo(""));
//		firstName.setIcon(FontAwesome.USER_MD);
//		firstName.setDescription("User first name");
//		firstName.setRequiredIndicatorVisible(true);
//		lastName.setWidth("100%");
//
//		HorizontalLayout buttonsBar = new HorizontalLayout();
//		buttonsBar.setWidth("100%");
//
//		
//		HorizontalLayout leftButtons = new HorizontalLayout();
//		leftButtons.setSpacing(true);
//		leftButtons.addComponents(btnSave, btnDelete);
//		
//		HorizontalLayout rightButtons = new HorizontalLayout();
//		rightButtons.setSpacing(true);
//		rightButtons.setWidth("100%");
//		rightButtons.addComponent(btnClose);
//		rightButtons.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
//		rightButtons.setComponentAlignment(btnClose, Alignment.BOTTOM_RIGHT);
//		buttonsBar.addComponents(leftButtons,rightButtons);
//		buttonsBar.setExpandRatio(rightButtons,1);
//
//		fl.addComponents(firstName,lastName,age);
//		addComponents(fl,buttonsBar);
//	}
//
//	@Override
//	public void enter(ViewChangeEvent event) {
//		String[] params = event.getParameters().split("/");
//		for (String str: params) {
//			System.out.println(str);
//		}
//	}
//
//	@Override
//	public void doSave() {	
//		presenter.doSave();
//	}
//
//	@Override
//	public void doDelete() {
//		presenter.doDelete();
//	}
//
//
//
//
//
//
//
//}
