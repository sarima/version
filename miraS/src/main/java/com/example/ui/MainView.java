package com.example.ui;

import com.example.ui.user.UserViewImpl;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class MainView extends VerticalLayout implements View{
	private static final long serialVersionUID = 1L;
	
	public MainView() {
		setSizeFull();
		Button btnUser = new Button("User");
		btnUser.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				doBtnUserClick();
			}
			
		});
		
		Button btnPerson = new Button("Person");
		btnPerson.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				doBtnPersonClick();
			}
			
		});
		addComponents(btnUser,btnPerson);
	}
	
	protected void doBtnPersonClick() {
	}

	@Override
	public void enter(ViewChangeEvent event) {		
	}
	protected void doBtnUserClick() {
		//UI.getCurrent().getNavigator().addView("user", UserViewImpl.class);
		UI.getCurrent().getNavigator().navigateTo("user");
	}
}
