package com.example.ui.user;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import com.example.entities.User;
import com.example.service.UserService;
import com.example.ui.base.AbstractBasePresenter;


public class UserPresenter extends AbstractBasePresenter{
	private UserView view;
	
	
	@Autowired
	private UserService service;
	
	
	public UserPresenter(UserView view) {
		super();
		this.view = view;	
		System.out.println(service.toString());
	}
	

	
	public void doSave() {
		User model = this.view.getModel();
		if(model.getId() != null) {
			User updatedUser = service.update(model);
			view.setModel(updatedUser);
		}
		else {
			User savedUser = service.save(model);
			view.setModel(savedUser);
		}
		
	}
	
	public void doDelete(User user) {
//		User model = this.view.getModel();
		service.delete(user);
	}

	public List<User> getAllUsers() {
		return service.findAll();
	}
	
	public List<User> findUsers(String firstName, String lastName) {
		return service.findByNameAndSurname(firstName, lastName);
	}



//	public void doUpdate() {
//		User model = this.view.getModel();
//		User updatedUser = service.update(model);
//		view.setModel(updatedUser);
//		//service.update(user);
//	}
	
	
}
