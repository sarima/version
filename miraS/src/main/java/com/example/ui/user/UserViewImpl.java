package com.example.ui.user;

import java.util.Collection;
import java.util.List;

import com.example.entities.User;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.themes.ValoTheme;

public class UserViewImpl extends VerticalLayout implements UserView {
	private static final long serialVersionUID = 1L;

	private TextField firstName = new TextField("First Name");
	private TextField lastName = new TextField("Last Name");
	private TextField fnSearch = new TextField();
	private TextField lnSearch = new TextField();
	private Button btnSave = new Button("Save");
	private Button btnNew = new Button("New");
	private Button btnClose = new Button("Close");
	private Button btnSearch = new Button();
	private UserPresenter presenter;
	private Binder<User> binder = new Binder<>(User.class);
	private User bean;
	Grid<User> users = new Grid<User>();

	public UserViewImpl() {
		this.presenter = new UserPresenter(this);
		bean = new User();
		buildUI();
		bindUI();
		gridUI();
	}

	private void buildUI() {
		setMargin(true);

		btnSave.addClickListener(event -> doSave());
		// btnDelete.addClickListener(event -> doDelete());
		btnNew.addClickListener(event -> doNew());

		fnSearch.setPlaceholder("first name search...");
		lnSearch.setPlaceholder("last name search...");

		// fnSearch.addValueChangeListener(e -> updateList());
		// fnSearch.setValueChangeMode(ValueChangeMode.LAZY);

		FormLayout fl = new FormLayout();
		FormLayout fl1 = new FormLayout();
		FormLayout fl2 = new FormLayout();

		btnClose.addClickListener(event -> UI.getCurrent().getNavigator().navigateTo(""));
		firstName.setIcon(FontAwesome.USER_MD);
		firstName.setDescription("User First Name");
		firstName.setRequiredIndicatorVisible(true);
		firstName.setMaxLength(30);
		lastName.setDescription("User Last Name");
		lastName.setRequiredIndicatorVisible(true);
		btnSearch.setIcon(FontAwesome.SEARCH);
		btnSearch.addClickListener(e -> doSearch());

		fl1.addComponents(fnSearch, lnSearch);
		fl2.addComponents(btnSearch);

		HorizontalLayout fs = new HorizontalLayout();
		HorizontalLayout rigth = new HorizontalLayout();
		rigth.setSpacing(true);
		rigth.addComponents(fl1, fl2);

		HorizontalLayout buttonsBar = new HorizontalLayout();
		buttonsBar.setWidth("100%");

		HorizontalLayout leftButtons = new HorizontalLayout();
		leftButtons.setSpacing(true);
		leftButtons.addComponents(btnSave, btnNew);

		HorizontalLayout rightButtons = new HorizontalLayout();
		rightButtons.setSpacing(true);
		// rightButtons.setWidth("100%");
		rightButtons.addComponent(btnClose);
		rightButtons.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
		// rightButtons.setComponentAlignment(btnClose, Alignment.BOTTOM_RIGHT);
		buttonsBar.addComponents(leftButtons, rightButtons);
		buttonsBar.setExpandRatio(rightButtons, 1);

		fl.addComponents(firstName, lastName);
		fs.addComponents(fl, rigth);
		addComponents(fs, buttonsBar);

	}


	private void doNew() {
		setModel(new User());
	}

	private void doSearch() {
		List<User> userList = presenter.findUsers(fnSearch.getValue(), lnSearch.getValue());
		users.setItems(userList);
	}

	private void bindUI() {
		// this.binder.forField(firstName).withConverter(Integer::valueOf, value ->
		// (value == null) ? "" : String.valueOf(value), "Must be a
		// number").bind(User::getUserName, User::setUserName);
		this.binder.forField(firstName)
				.withValidator(new StringLengthValidator("Error: User FirstName must be 3-30", 3, 30))
				.bind("firstName");
		this.binder.forField(lastName)
				.withValidator(new StringLengthValidator("Error: User LastName must be 3-30 (was {0})", 3, 30))
				.bind(User::getLastName, User::setLastName);
	}

	public void gridUI() {
		List<User> list = presenter.getAllUsers();
		
		users.setWidth("400px");
		users.addColumn(User::getId).setCaption("ID");
		users.addColumn(User::getFirstName).setCaption("Name");
		users.addColumn(User::getLastName).setCaption("LastName");
		//users.addColumn(users -> "Delete", new ButtonRenderer(e -> doDelete(e))).setCaption("Delete");
		//users.addColumn(users -> "Edit", new ButtonRenderer(e -> doEdit(e))).setCaption("Edit");
		users.addComponentColumn(this::HL);
		users.setItems((Collection<User>) list);
		addComponent(users);
	}
	
	private HorizontalLayout HL(User p) {
		HorizontalLayout HL = new HorizontalLayout();
		HL.addComponent(buildDeleteButton(p));
		HL.addComponent(buildEditButton(p));
		return HL;
	}

	private Component buildEditButton(User p) {
		Button button = new Button(VaadinIcons.EDIT);
		button.addStyleName(ValoTheme.BUTTON_SMALL);
		button.addClickListener(e-> doEdit(p));
		return button;
	}

	private Component buildDeleteButton(User p) {
		Button button = new Button(VaadinIcons.CLOSE);
		button.addStyleName(ValoTheme.BUTTON_SMALL);
		button.addClickListener(e->doDelete(p));
		return button;
	}

	public void doEdit(User p) {
		users.select(p);
		setModel(p);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		String[] params = event.getParameters().split("/");
		for (String str : params) {
			System.out.println(str);
		}
	}

	@Override
	public void doSave() {
		try {
			binder.writeBean(bean);
			presenter.doSave();
			List<User> list = presenter.getAllUsers();
			users.setItems((Collection<User>) list);
			users.getDataProvider().refreshAll();
		} catch (ValidationException e) {
			Notification.show("Person could not be saved, " + "please check error messages for each field.");
		}

	}

//	@Override
//	public void doDelete(RendererClickEvent<User> e) {
//		users.select(e.getItem());
//		if (users.getSelectedItems().iterator().hasNext()) {
//
//			presenter.doDelete(users.getSelectedItems().iterator().next());
//
//			List<User> list = presenter.getAllUsers();
//			users.setItems((Collection<User>) list);
//
//			users.getDataProvider().refreshAll();
//		}
//	}
	
	@Override
	public void doDelete(User p) {
		users.select(p);
		if (users.getSelectedItems().iterator().hasNext()) {

			presenter.doDelete(users.getSelectedItems().iterator().next());

			List<User> list = presenter.getAllUsers();
			users.setItems((Collection<User>) list);

			users.getDataProvider().refreshAll();
		}
	}

	@Override
	public User getModel() {
		return bean;
	}

	@Override
	public void setModel(User bean) {
		this.bean = bean;
		binder.setBean(bean);
	}

}
