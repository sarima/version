package com.example.ui.user;

import com.example.entities.User;
import com.vaadin.navigator.View;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;

public interface UserView extends View {
	public void doSave();

	public User getModel();

	public void setModel(User bean);

	void doDelete(User p);

//	void doDelete(RendererClickEvent<User> e);
}
