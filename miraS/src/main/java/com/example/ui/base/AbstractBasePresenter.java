package com.example.ui.base;

public abstract class AbstractBasePresenter {
	public AbstractBasePresenter() {
		MvpInjector.Inject(this);
	}
}
