package com.example.config;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.example.ui.base.MvpInjector;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
public class DemoConfig {

	@Autowired
	private Environment environment;
	
	@Autowired
	private ApplicationContext context;
	
	@PostConstruct
	public void init() {
		MvpInjector.context = context;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource(environment));
		sessionFactory.setPackagesToScan(new String[] { "com.example.entities" });
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
		return properties;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}

	@Bean
	public DataSource dataSource(Environment environment) {
		//HikariConfig dataSourceConfig = new HikariConfig();
		BasicDataSource dataSourceConfig = new BasicDataSource();
		dataSourceConfig.setDriverClassName(environment.getRequiredProperty("db.driver"));
		dataSourceConfig.setUrl(environment.getRequiredProperty("db.url"));
		dataSourceConfig.setUsername(environment.getRequiredProperty("db.username"));
		dataSourceConfig.setPassword(environment.getRequiredProperty("db.password"));
		return dataSourceConfig;
	}
	
	
}
