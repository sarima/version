package com.example;


import com.example.ui.MainView;
import com.example.ui.user.UserViewImpl;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SpringUI
@Theme("valo")
@UIScope
public class ProjectUI extends UI{
	private static final long serialVersionUID = 1L;

	private Navigator navigator;
	
	@Override
	protected void init(VaadinRequest request) {
		navigator = new Navigator(this,this);
		navigator.addView("", new MainView());
		navigator.addView("user", UserViewImpl.class);
	}
	
	public Navigator getNavigator() {
		return navigator;
	}
}
