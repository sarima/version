	package com.example.service;

import java.util.List;

import com.example.entities.User;

public interface UserService {

	User save(User entity);

	User update(User entity);

	boolean delete(User entity);

	List<User> findAll();
	
	List<User> findByNameAndSurname(String firstName, String lastName);

}