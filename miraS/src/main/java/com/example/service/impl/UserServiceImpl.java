package com.example.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UserDao;
import com.example.dao.impl.UserDaoImpl;
import com.example.entities.User;
import com.example.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Override
	public User save(User entity) {
		return dao.save(entity);
	}

	@Override
	public User update(User entity) {
		return dao.update(entity);
	}

	@Override
	public boolean delete(User entity) {
		return dao.delete(entity);
	}
	
	@Override
	public List<User> findAll() {
		return dao.findAll();
	}

	@Override
	public List<User> findByNameAndSurname(String firstName, String lastName) {
		return dao.findByNameAndSurname(firstName, lastName);
	}


}
