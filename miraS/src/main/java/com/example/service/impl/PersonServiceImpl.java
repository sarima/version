package com.example.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PersonDao;
import com.example.entities.Person;
import com.example.service.PersonService;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {
	
	@Autowired
	private PersonDao dao;

	@Override
	public Person save(Person entity) {
		return dao.save(entity);
	}

	@Override
	public Person update(Person entity) {
		return dao.update(entity);
	}

	@Override
	public boolean delete(Person entity) {
		return dao.delete(entity);
	}

}
