package com.example.service;

import com.example.entities.Person;

public interface PersonService {

	Person save(Person entity);

	Person update(Person entity);

	boolean delete(Person entity);

}