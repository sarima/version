package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.example.config.DemoConfig;



@SpringBootApplication
@Import(value={DemoConfig.class})
@ComponentScan(basePackages= {"com.example", "com.example.ui.user"})
public class ProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}
}

//@SpringBootApplication
//@Import(value={AppConfig.class})
//public class ProjectApplication {
//   public static void main(String[] args){
//       SpringApplication.run(ProjectApplication.class, args);
//
//   }
//}
