package com.example.dao;

import com.example.dao.generic.BaseDao;
import com.example.entities.Person;

public interface PersonDao extends BaseDao<Person,Long>{

}