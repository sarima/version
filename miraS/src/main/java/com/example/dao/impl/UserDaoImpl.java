package com.example.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.example.dao.UserDao;
import com.example.dao.generic.AbstractBaseDao;
import com.example.entities.User;

@Repository
public class UserDaoImpl extends AbstractBaseDao<User, Long> implements UserDao{
	
	@PersistenceContext
    private EntityManager em;

	public UserDaoImpl() {
		this.type = User.class;
	}

	@Override
	public List<User> findByNameAndSurname(String name, String surname) {
//		CriteriaBuilder builder = em.getCriteriaBuilder();
//        CriteriaQuery<User> query = builder.createQuery(User.class);
//        Root<User> root = query.from(User.class);
//        query.where(builder.and(builder.like(root.get("firstName"), "%" + name + "%"), builder.like(root.get("lastName"), "%" + surname + "%")));
//        TypedQuery<User> q = em.createQuery(query);
        Query q2 = em.createQuery("SELECT u FROM User u WHERE u.firstName LIKE '%"+name+"%'");
        List<User> list = q2.getResultList();
        return list;
	}

}
