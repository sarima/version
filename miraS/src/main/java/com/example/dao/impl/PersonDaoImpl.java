package com.example.dao.impl;

import org.springframework.stereotype.Repository;

import com.example.dao.PersonDao;
import com.example.dao.generic.AbstractBaseDao;
import com.example.entities.Person;

@Repository
public class PersonDaoImpl extends AbstractBaseDao<Person,Long> implements PersonDao{

	public PersonDaoImpl() {
		this.type = Person.class;
	}
}
