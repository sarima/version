package com.example.dao;

import java.util.List;

import com.example.dao.generic.BaseDao;
import com.example.entities.User;

public interface UserDao extends BaseDao<User, Long>{

	public List<User> findByNameAndSurname(String name, String surname);

}