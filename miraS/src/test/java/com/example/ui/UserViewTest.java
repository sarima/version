package com.example.ui;

import com.example.entities.User;
import com.example.ui.user.UserPresenter;
import com.example.ui.user.UserView;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;

public class UserViewTest implements UserView{
	private static final long serialVersionUID = 1L;
	private User user;
	private UserPresenter presenter;

	public UserViewTest() {
		this.presenter = new UserPresenter(this);
	}
	
	@Override
	public void doSave() {	
		this.presenter.doSave();
	}

//	public void doDelete() {		
//		this.presenter.doDelete(user);
//	}

	@Override
	public User getModel() {
		user = new User();
		user.setId(1L);
		user.setFirstName("Ahmad");
		user.setLastName("Admin");
		return user;
	}

	@Override
	public void setModel(User bean) {	
		this.user = bean;
	}

	@Override
	public void doDelete(User p) {
		// TODO Auto-generated method stub
		
	}

}
